/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ldnr.helloWorld;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebInitParam;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author stag
 */
//Avec les annotations, le servlet n'apparait pas dans le web.xml
@WebServlet(name = "MonTroisiemeServlet", urlPatterns = {"/troisieme"},initParams ={@WebInitParam(name = "prenom", value="bob")})

public class MonTroisiemeServletCookies extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String prenom;
        if(request.getParameter("prenom") != null){
            prenom = request.getParameter("prenom");
        }
        else{
                prenom = getInitParameter("prenom");
        }
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MonTroisiemeServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Mon servlet a cookies</h1>");
           Cookie cookie=new Cookie("nom", "elalaoui");//creating cookie object 
            Cookie[] cookies = request.getCookies();
            response.addCookie(cookie);
        if (cookies != null) {
            for (Cookie ck : cookies) {
                out.print("<h2>Voici mon cookie :"+cookies[0].getName()+" "+ cookies[0].getValue()+"</h2>");  
            }
        }   out.print("<h1>Bonjour: "+ prenom +"</h1>");  
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
